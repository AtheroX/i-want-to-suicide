﻿using UnityEngine;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.U2D;

public class InGameManager : MonoBehaviourPun {

    [System.Serializable]
    public class Map {
        public string name;
        public bool active;
        public int id;
        public GameObject map;
        public int x;
        public int y;
    }

    private InGameManager instance;
    public GameObject PlayerOnline;
    public GameObject PlayerOffline;
    public bool playercreated = false;
    public Vector3[] Spawnpoints = new Vector3[4];
    [Space]
    public List<Map> Maps = new List<Map>();
    public Map SelectedMap;

    ExitGames.Client.Photon.Hashtable CustomValue; //Para crear values en el server


    void Awake() {
        if (instance) {
            Destroy(gameObject);
        } else {
            instance = this;
        }

        if (!playercreated){
            if (!PhotonNetwork.IsConnected) {
                SelectRandomMap();
                CreateMap();
                PhotonNetwork.OfflineMode = true;
                Instantiate(PlayerOffline);
                playercreated = true;
                ReposCamera();
            } else {
                if (PhotonNetwork.IsMasterClient) {
                    CustomValue = new ExitGames.Client.Photon.Hashtable();
                    SelectRandomMapOnline();
                }
                CreateMapOnline();
                ReposCamera();
                PhotonNetwork.Instantiate(PlayerOnline.name, Spawnpoints[(int)PlayerOnline.GetComponent<PhotonView>().ViewID/1000-1], Quaternion.identity);
                playercreated = true;
            }
        }
    }

    public void SelectRandomMap() {
        int rng = UnityEngine.Random.Range(0, Maps.Count);
        Debug.Log(rng.ToString());
        foreach (var i in Maps) {
            if (i.id == rng) {
                SelectedMap = i;
            }
        }
    }

    public void SelectRandomMapOnline() {
        int rng = UnityEngine.Random.Range(0, Maps.Count);
        CustomValue.Add("RandomMapID", rng);
        PhotonNetwork.CurrentRoom.SetCustomProperties(CustomValue);
    }

    public void CreateMap() {
        Instantiate(SelectedMap.map);
    }

    public void CreateMapOnline() {
        SelectedMap = Maps[(int)PhotonNetwork.CurrentRoom.CustomProperties["RandomMapID"]];
        Instantiate(SelectedMap.map);
    }

    public void ReposCamera() {
        PixelPerfectCamera cam = Camera.main.GetComponent<PixelPerfectCamera>();
        cam.refResolutionX = SelectedMap.x;
        cam.refResolutionY = SelectedMap.y;
    }
}
